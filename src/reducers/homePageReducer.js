
let userlistPageData = {
    usersList: [],
    showLoader: false
}
export default function getUsersReducer(state = userlistPageData, action) {
    switch (action.type) {
        case "FETCH_USERS": {
            return {...state, showLoader: true }
        }
        case "FETCH_USERS_SUCCESS": {
            return {...state, showLoader: false, usersList: action.payload }
        }

        default : 
            return state;
    }
} 