let userLogin = {
    userObj: {},
    showLoader: false
}

export default function loginPageStore(state = userLogin, action) {
    switch (action.type) {
        case "SUBMIT_USER": {
            return {...state, showLoader: true}
        }
        case "SUBMIT_USER_SUCCESS": {
            return {...state, showLoader: false, userObj:{}}
        }
        
        default :
            return state;
    }
}