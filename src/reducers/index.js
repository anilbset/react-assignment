import {combineReducers} from 'redux';
import homePageReducer from './homePageReducer';
import loginPageReducer from './loginPageReducer';

const combinedReducers = combineReducers({
    homePageStore: homePageReducer,
    loginPageStore: loginPageReducer

})

export default combinedReducers;