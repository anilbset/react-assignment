import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

/**
 * Import home page, which contains the list of projects
 */
import HomePage from "./containers/HomePage";
import Login from "./containers/Login";

const App = () => (
  <Router>
    <Switch>
      <Route exact path='/' component={HomePage} />
      <Route path='/login' component={Login} />
      
      <Route path='*' render={() => <div>Page Not Found <div> <Link to='/'>Click here to Home Page</Link></div></div>} />
    </Switch>
  </Router>
);

export default App;
