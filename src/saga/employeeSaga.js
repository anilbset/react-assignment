import { put, takeEvery, call } from 'redux-saga/effects';
import {fetchUsersApi, submitLoginApi} from '../api'

function* fetchEmployees(){
    const data = yield call(fetchUsersApi)
    yield put({type:"FETCH_USERS_SUCCESS", payload: data})
}

function* submitLogin(action){
    const data = yield call(submitLoginApi, action.payload);
    yield put({type:"FETCH_USERS_SUCCESS", payload: data});
}

function* employeeSaga() {
    yield takeEvery('FETCH_USERS', fetchEmployees);
    yield takeEvery("SUBMIT_LOGIN", submitLogin)
}

export default employeeSaga;